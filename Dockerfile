FROM python:3.11.3-alpine3.17 as builder

ENV PYTHONUSERBASE /app
ENV PATH "$PATH:/app/bin"

WORKDIR /app

COPY ./requirements.txt requirements.txt

RUN apk update && apk upgrade && \
    apk add --no-cache gcc \
    musl-dev \
    oniguruma-dev && \
    python3 -m pip install --user --no-cache-dir --upgrade pip && \
    python3 -m pip install --user --no-cache-dir --only-binary :all: cryptography && \
    python3 -m pip install --user --no-cache-dir --prefer-binary -r requirements.txt

FROM python:3.11.3-alpine3.17

RUN apk add --no-cache git

ENV PYTHONUSERBASE /app
ENV PATH "$PATH:/app/bin"
COPY --from=builder /app /app

ENTRYPOINT ["ansible-lint"]
WORKDIR /code/
