# ansible lint

This is a docker image with the `ansible-lint` python package in it. It is used
to lint ansible projects.

It is based on an alpine variant of the official [python 3.11
image](https://hub.docker.com/_/python) from dockerhub.

The image was inspired by the
[ansible-lint](https://gitlab.com/pipeline-components/ansible-lint) image from
gitlab's pipeline components.

### How to run
To run the image.

Go to the ansible directory containing the roles and playbooks and run the
below command
```bash
# To lint the whole project
docker run --rm -i -v $(pwd):/code/ 

# To lint a specific role,playbook,collection,file
docker run --rm -i -v $(pwd):/code/ <path/to/directory>
```
